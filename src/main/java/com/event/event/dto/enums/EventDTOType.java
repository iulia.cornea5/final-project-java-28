package com.event.event.dto.enums;

public enum EventDTOType {
    CONCERT, SPORTS, THEATRE, PARTY
}
