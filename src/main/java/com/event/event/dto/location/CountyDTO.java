package com.event.event.dto.location;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;
@AllArgsConstructor
@Getter
public class CountyDTO {

    private UUID uuid;

    private String name;

    private CountryDTO countryDTO;
}
