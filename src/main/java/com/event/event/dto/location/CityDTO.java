package com.event.event.dto.location;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CityDTO {

    private UUID uuid;

    private String name;

    private String postalCode;

    private CountyDTO countyDTO;

}
