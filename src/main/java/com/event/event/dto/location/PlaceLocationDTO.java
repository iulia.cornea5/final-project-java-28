package com.event.event.dto.location;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@Getter
public class PlaceLocationDTO {

    private UUID uuid;

    private String name;

    private Long latitude;

    private Long longitude;

    private Integer totalCapacity;

    private CityDTO cityDTO;

    private List<VenueLocationDTO> venues;

}
