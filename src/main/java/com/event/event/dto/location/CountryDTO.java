package com.event.event.dto.location;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class CountryDTO {

    private String code;

    private String name;

    private ContinentDTO continentDTO;
}
