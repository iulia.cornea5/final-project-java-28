package com.event.event.controller;

import com.event.event.dto.location.PlaceLocationDTO;
import com.event.event.exceptions.ResourceNotFoundException;
import com.event.event.service.location.PlaceService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/api/places")
public class PlaceController {

    private PlaceService placeService;

    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping
    ResponseEntity<List<PlaceLocationDTO>> getAll() {
        return new ResponseEntity<>(placeService.findAll(), HttpStatus.FOUND);
    }

    @PostMapping
    ResponseEntity<PlaceLocationDTO> create(@RequestBody PlaceLocationDTO placeLocationDTO) {
        return new ResponseEntity<>(placeService.save(placeLocationDTO), HttpStatus.CREATED);
    }

    // TODO discuss logic and code quality
    @PostMapping(path = "/{uuid}")
    ResponseEntity<PlaceLocationDTO> update(@PathVariable String uuid, @RequestBody PlaceLocationDTO placeLocationDTO) {
        if (placeLocationDTO.getUuid().toString().equals(uuid) && placeService.checkExistsById(UUID.fromString(uuid))) {
            return new ResponseEntity<>(placeService.save(placeLocationDTO), HttpStatus.CREATED);
        } else {
            throw new ResourceNotFoundException("Place", "uuid", uuid);
        }
    }
}
