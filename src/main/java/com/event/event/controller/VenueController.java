package com.event.event.controller;

import com.event.event.dto.location.VenueLocationDTO;
import com.event.event.exceptions.ResourceNotFoundException;
import com.event.event.service.location.VenueService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/venues")
public class VenueController {

    private VenueService venueService;

    public VenueController(VenueService venueService) {
        this.venueService = venueService;
    }

    @GetMapping
    ResponseEntity<List<VenueLocationDTO>> getAll() {
        return new ResponseEntity<>(venueService.findAll(), HttpStatus.FOUND);
    }

    @PostMapping
    ResponseEntity<VenueLocationDTO> create(@RequestBody VenueLocationDTO venueLocationDTO) {
        return new ResponseEntity<>(venueService.save(venueLocationDTO), HttpStatus.CREATED);
    }

    // TODO discuss logic and code quality
    @PostMapping(path = "/{uuid}")
    ResponseEntity<VenueLocationDTO> update(@PathVariable String uuid, @RequestBody VenueLocationDTO venueLocationDTO) {
        if(venueLocationDTO.getUuid().toString().equals(uuid) && venueService.checkExistsById(UUID.fromString(uuid))) {
            return new ResponseEntity<>(venueService.save(venueLocationDTO), HttpStatus.CREATED);
        } else {
            throw new ResourceNotFoundException("Venue", "uuid", uuid);
        }
    }
}
