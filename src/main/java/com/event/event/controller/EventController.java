package com.event.event.controller;

import com.event.event.dto.EventDTO;
import com.event.event.entity.Event;
import com.event.event.mapper.EventMapper;
import com.event.event.service.EventService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/events")
public class EventController {

    private EventService service;

    public EventController(EventService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<EventDTO>> getAll() {
        return new ResponseEntity<List<EventDTO>>(service.findAll(), HttpStatus.FOUND);
    }

    @PostMapping
    public ResponseEntity<EventDTO> create(@RequestBody EventDTO eventDTO) {
        return new ResponseEntity<EventDTO>(service.saveEvent(eventDTO), HttpStatus.CREATED);
    }

    @PostMapping("/{uuid}")
    public ResponseEntity<EventDTO> update(@PathVariable String uuid, @RequestBody EventDTO eventDTO) {
        // TODO discuss how to implement
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    // TODO discuss delete methods
    @DeleteMapping
    public ResponseEntity<UUID> delete(@RequestBody EventDTO eventDTO) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }

    @DeleteMapping("/{uuid}")
    public ResponseEntity<UUID> delete(@PathVariable String uuid) {
        return new ResponseEntity<>(HttpStatus.NOT_IMPLEMENTED);
    }
}
