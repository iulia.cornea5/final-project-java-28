package com.event.event.entity.location;

public interface Location {

    String getName();

    Integer getTotalCapacity();

}
