package com.event.event.entity.location;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Venue implements Location{

    @Id
    @GeneratedValue
    private UUID uuid;

    private String name;

    private Integer totalCapacity;

    @ManyToOne
    private Place place;
}
