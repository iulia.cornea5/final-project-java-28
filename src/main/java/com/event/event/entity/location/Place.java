package com.event.event.entity.location;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Place implements Location {

    @Id
    @GeneratedValue
    private UUID uuid;
    private String name;
    private Long latitude;
    private Long longitude;

    @ManyToOne
    private City city;

    @OneToMany(mappedBy = "place")
    private List<Venue> venues;

    @Override
    public Integer getTotalCapacity() {
        return venues.stream().map(v -> v.getTotalCapacity()).reduce(0, (sum, capacity) -> sum + capacity);
    }
}
