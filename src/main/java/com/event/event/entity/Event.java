package com.event.event.entity;

import com.event.event.entity.enums.EventType;
import com.event.event.entity.location.Location;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor // We need this for the Hibernate update to work (Hibernate needs to find a default constructor, this will provide the no args constructor as the default)
@Data // Equivalent to @Getter @Setter @RequiredArgsConstructor @ToString @EqualsAndHashCode.
@Entity // this will make hibernate to actually create the table
// @Table can be omitted if we are fine with the default specifications
public class Event {

    @Id
    @GeneratedValue
    private UUID id;

    @NonNull
    private String name;

    @Enumerated(EnumType.STRING)
    private EventType eventType;

    private Date startDate;


}
