package com.event.event.entity.enums;

public enum EventType {
    CONCERT, SPORTS, THEATRE, PARTY
}
