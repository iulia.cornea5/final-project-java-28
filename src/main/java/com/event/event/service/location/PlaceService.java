package com.event.event.service.location;

import com.event.event.dto.location.PlaceLocationDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.UUID;

public interface PlaceService {

    PlaceLocationDTO save(PlaceLocationDTO dto);

    List<PlaceLocationDTO> findAll();

    boolean checkExistsById(UUID uuid);
}
