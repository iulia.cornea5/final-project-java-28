package com.event.event.service.location;

import com.event.event.dto.location.VenueLocationDTO;

import java.util.List;
import java.util.UUID;

public interface VenueService {

    List<VenueLocationDTO> findAll();

    VenueLocationDTO save(VenueLocationDTO venueLocationDTO);

    boolean checkExistsById(UUID uuid);
}
