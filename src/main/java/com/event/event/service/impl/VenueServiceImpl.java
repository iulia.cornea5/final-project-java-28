package com.event.event.service.impl;

import com.event.event.dto.location.VenueLocationDTO;
import com.event.event.mapper.location.LocationMapper;
import com.event.event.repo.location.VenueRepo;
import com.event.event.service.location.VenueService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class VenueServiceImpl implements VenueService {

    private LocationMapper locationMapper;
    private VenueRepo venueRepo;

    public VenueServiceImpl(LocationMapper locationMapper, VenueRepo venueRepo) {
        this.locationMapper = locationMapper;
        this.venueRepo = venueRepo;
    }

    @Override
    public List<VenueLocationDTO> findAll() {
        return venueRepo.findAll().stream().map(locationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public VenueLocationDTO save(VenueLocationDTO venueLocationDTO) {
        return locationMapper.toDto(venueRepo.save(locationMapper.toEntity(venueLocationDTO)));
    }

    @Override
    public boolean checkExistsById(UUID uuid) {
        return venueRepo.existsById(uuid);
    }
}
