package com.event.event.service.impl;

import com.event.event.dto.EventDTO;
import com.event.event.entity.Event;
import com.event.event.mapper.EventMapper;
import com.event.event.repo.EventRepo;
import com.event.event.service.EventService;
import org.springframework.stereotype.Service;

import javax.persistence.Entity;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl implements EventService {

    private EventRepo repo;
    private EventMapper mapper;

    public EventServiceImpl(EventRepo repo, EventMapper mapper) {
        this.repo = repo;
        this.mapper = mapper;
    }

    @Override
    public EventDTO saveEvent(EventDTO dto) {
//        Event entityToSave = mapper.toEntity(dto);
//        Event savedEntity =  repo.save(entityToSave);
//        return mapper.toDto(savedEntity);
        return mapper.toDto(repo.save(mapper.toEntity(dto)));
    }

    @Override
    public List<EventDTO> findAll() {
        return repo.findAll().stream().map(mapper::toDto).collect(Collectors.toList());
    }
}
