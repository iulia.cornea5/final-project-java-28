package com.event.event.service.impl;

import com.event.event.dto.location.PlaceLocationDTO;
import com.event.event.entity.location.Place;
import com.event.event.mapper.location.LocationMapper;
import com.event.event.repo.location.PlaceRepo;
import com.event.event.service.location.PlaceService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PlaceServiceImpl implements PlaceService {

    private PlaceRepo placeRepo;
    private LocationMapper locationMapper;

    public PlaceServiceImpl(PlaceRepo placeRepo, LocationMapper locationMapper) {
        this.placeRepo = placeRepo;
        this.locationMapper = locationMapper;
    }

    @Override
    public PlaceLocationDTO save(PlaceLocationDTO dto) {
        return locationMapper.toDto(placeRepo.save(locationMapper.toEntity(dto)));
    }

    @Override
    public List<PlaceLocationDTO> findAll() {
        List<Place> places = placeRepo.findAll();
        return places.stream().map(locationMapper::toDto).collect(Collectors.toList());
    }

    @Override
    public boolean checkExistsById(UUID uuid) {
        return placeRepo.existsById(uuid);
    }

}
