package com.event.event.mapper.location;

import com.event.event.dto.location.CountyDTO;
import com.event.event.entity.location.County;

public interface CountyMapper {


    CountyDTO toDto(County entity);
}
