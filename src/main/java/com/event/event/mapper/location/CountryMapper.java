package com.event.event.mapper.location;

import com.event.event.dto.location.CountryDTO;
import com.event.event.entity.location.Country;

public interface CountryMapper {


    CountryDTO toDto(Country entity);
}
