package com.event.event.mapper.location;

import com.event.event.dto.location.CityDTO;
import com.event.event.entity.location.City;

public interface CityMapper {

    CityDTO toDto(City entity);
}
