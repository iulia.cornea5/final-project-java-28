package com.event.event.mapper.location;

import com.event.event.dto.location.ContinentDTO;
import com.event.event.entity.location.Continent;

public interface ContinentMapper {

    ContinentDTO toDto(Continent entity);
}
