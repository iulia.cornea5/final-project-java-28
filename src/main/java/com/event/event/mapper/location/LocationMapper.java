package com.event.event.mapper.location;

import com.event.event.dto.location.PlaceLocationDTO;
import com.event.event.dto.location.VenueLocationDTO;
import com.event.event.entity.location.Place;
import com.event.event.entity.location.Venue;

public interface LocationMapper {

    Place toEntity(PlaceLocationDTO dto);

    PlaceLocationDTO toDto(Place entity);

    Venue toEntity(VenueLocationDTO dto);

    VenueLocationDTO toDto(Venue entity);

}
