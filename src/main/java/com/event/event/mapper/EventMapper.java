package com.event.event.mapper;

import com.event.event.dto.EventDTO;
import com.event.event.entity.Event;

public interface EventMapper {

    Event toEntity(EventDTO dto);

    EventDTO toDto(Event entity);
}
