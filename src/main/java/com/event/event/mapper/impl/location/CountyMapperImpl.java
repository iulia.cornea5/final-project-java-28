package com.event.event.mapper.impl.location;

import com.event.event.dto.location.CountyDTO;
import com.event.event.entity.location.County;
import com.event.event.mapper.location.CountryMapper;
import com.event.event.mapper.location.CountyMapper;
import org.springframework.stereotype.Component;

@Component
public class CountyMapperImpl implements CountyMapper {

    private CountryMapper countryMapper;

    public CountyMapperImpl(CountryMapper countryMapper) {
        this.countryMapper = countryMapper;
    }

    @Override
    public CountyDTO toDto(County entity) {
        return new CountyDTO(entity.getUuid(), entity.getName(), countryMapper.toDto(entity.getCountry()));
    }
}
