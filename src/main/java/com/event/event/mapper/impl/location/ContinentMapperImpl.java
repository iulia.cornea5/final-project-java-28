package com.event.event.mapper.impl.location;

import com.event.event.dto.location.ContinentDTO;
import com.event.event.entity.location.Continent;
import com.event.event.mapper.location.ContinentMapper;
import org.springframework.stereotype.Component;

@Component
public class ContinentMapperImpl implements ContinentMapper {

    @Override
    public ContinentDTO toDto(Continent entity) {
        return new ContinentDTO(entity.getName());
    }
}
