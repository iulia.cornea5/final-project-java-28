package com.event.event.mapper.impl.location;

import com.event.event.dto.location.PlaceLocationDTO;
import com.event.event.dto.location.VenueLocationDTO;
import com.event.event.entity.location.Place;
import com.event.event.entity.location.Venue;
import com.event.event.mapper.location.CityMapper;
import com.event.event.mapper.location.LocationMapper;
import com.event.event.repo.location.CityRepo;
import com.event.event.repo.location.PlaceRepo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class LocationMapperImpl implements LocationMapper {

    private CityMapper cityMapper;
    private CityRepo cityRepo;
    private PlaceRepo placeRepo;

    public LocationMapperImpl(CityMapper cityMapper, CityRepo cityRepo, PlaceRepo placeRepo) {
        this.cityMapper = cityMapper;
        this.cityRepo = cityRepo;
        this.placeRepo = placeRepo;
    }

    @Override
    public Place toEntity(PlaceLocationDTO dto) {
        List<Venue> venues = dto.getVenues().stream().map(this::toEntity).collect(Collectors.toList());
        Place entity = new Place(
                dto.getUuid(),
                dto.getName(),
                dto.getLatitude(),
                dto.getLongitude(),
                cityRepo.getById(dto.getCityDTO().getUuid()),
                venues);
        return entity;
    }

    @Override
    public PlaceLocationDTO toDto(Place entity) {
        List<VenueLocationDTO> venueLocationDTOS = entity.getVenues().stream().map(this::toDto).collect(Collectors.toList());
        PlaceLocationDTO placeLocationDTO = new PlaceLocationDTO(
                entity.getUuid(),
                entity.getName(),
                entity.getLatitude(),
                entity.getLongitude(),
                entity.getTotalCapacity(),
                cityMapper.toDto(entity.getCity()),
                venueLocationDTOS);
        return placeLocationDTO;
    }

    @Override
    public Venue toEntity(VenueLocationDTO dto) {
        Place place = placeRepo.getById(dto.getPlaceLocationId());
        Venue entity = new Venue(
                dto.getUuid(),
                dto.getName(),
                dto.getTotalCapacity(),
                place);
        return entity;
    }

    @Override
    public VenueLocationDTO toDto(Venue entity) {
        VenueLocationDTO venueLocationDTO = new VenueLocationDTO(
                entity.getUuid(),
                entity.getName(),
                entity.getTotalCapacity(),
                entity.getPlace().getUuid());
        return venueLocationDTO;
    }

}
