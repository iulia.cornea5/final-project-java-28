package com.event.event.mapper.impl.location;

import com.event.event.dto.location.CountryDTO;
import com.event.event.entity.location.Country;
import com.event.event.mapper.location.ContinentMapper;
import com.event.event.mapper.location.CountryMapper;
import org.springframework.stereotype.Component;

@Component
public class CountryMapperImpl implements CountryMapper {

    private ContinentMapper continentMapper;

    public CountryMapperImpl(ContinentMapper continentMapper) {
        this.continentMapper = continentMapper;
    }

    @Override
    public CountryDTO toDto(Country entity) {
        return new CountryDTO(entity.getCode(), entity.getName(), continentMapper.toDto(entity.getContinent()));
    }
}
