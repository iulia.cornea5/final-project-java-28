package com.event.event.mapper.impl.location;

import com.event.event.dto.location.CityDTO;
import com.event.event.entity.location.City;
import com.event.event.mapper.location.CityMapper;
import com.event.event.mapper.location.CountyMapper;
import org.springframework.stereotype.Component;

@Component
public class CityMapperImpl implements CityMapper {

    private CountyMapper countyMapper;

    public CityMapperImpl(CountyMapper countyMapper) {
        this.countyMapper = countyMapper;
    }

    @Override
    public CityDTO toDto(City entity) {
        return new CityDTO(entity.getUuid(), entity.getName(), entity.getPostalCode(), countyMapper.toDto(entity.getCounty()));
    }
}
