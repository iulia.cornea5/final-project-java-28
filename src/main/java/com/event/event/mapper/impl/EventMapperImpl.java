package com.event.event.mapper.impl;

import com.event.event.dto.EventDTO;
import com.event.event.dto.enums.EventDTOType;
import com.event.event.entity.Event;
import com.event.event.entity.enums.EventType;
import com.event.event.mapper.EventMapper;
import org.springframework.stereotype.Component;

@Component
public class EventMapperImpl implements EventMapper {

    @Override
    public Event toEntity(EventDTO dto) {
        return new Event(
                dto.getId(),
                dto.getName(),
                EventType.valueOf(dto.getEventDTOType().toString()),
                dto.getStartDate());
    }

    @Override
    public EventDTO toDto(Event entity) {
        return new EventDTO(
                entity.getId(),
                entity.getName(),
                EventDTOType.valueOf(entity.getEventType().toString()),
                entity.getStartDate());
    }
}
