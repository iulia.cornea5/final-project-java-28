package com.event.event.repo.location;

import com.event.event.entity.location.City;
import com.event.event.entity.location.Place;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface PlaceRepo extends JpaRepository<Place, UUID> {

}
