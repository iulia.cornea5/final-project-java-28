package com.event.event.repo.location;

import com.event.event.entity.location.Continent;
import com.event.event.entity.location.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepo extends JpaRepository<Country, String> {
}
