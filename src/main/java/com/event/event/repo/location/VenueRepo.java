package com.event.event.repo.location;

import com.event.event.entity.location.Place;
import com.event.event.entity.location.Venue;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface VenueRepo extends JpaRepository<Venue, UUID> {
}
