package com.event.event.repo.location;

import com.event.event.entity.location.Continent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContinentRepo extends JpaRepository<Continent, String> {
}
