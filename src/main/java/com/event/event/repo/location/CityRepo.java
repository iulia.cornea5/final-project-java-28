package com.event.event.repo.location;

import com.event.event.entity.location.City;
import com.event.event.entity.location.County;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CityRepo extends JpaRepository<City, UUID> {
}
