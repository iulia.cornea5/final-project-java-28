package com.event.event.repo.location;

import com.event.event.entity.location.Country;
import com.event.event.entity.location.County;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface CountyRepo extends JpaRepository<County, UUID> {
}
