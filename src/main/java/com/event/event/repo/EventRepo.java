package com.event.event.repo;

import com.event.event.entity.Event;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

// No need to add @Repository and @Transactional annotation
// because they are present in SimpleJpaRepository
public interface EventRepo extends JpaRepository<Event, UUID> {
}
